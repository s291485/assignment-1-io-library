section .text
; Принимает код возврата и завершает текущий процесс
exit:
    mov rax, 60 ; exit syscall 
    xor rdi, rdi ; 0->rdi
    syscall
; Принимает указатель на нуль-терминированную строку, возвращает её длину
string_length:
xor rax, rax 
.loop:                  ;цикл loop пока byte[rdi+rax]!=0
    cmp byte[rdi+rax],0
    je .end
    inc rax             ; rax+1->rax
    jmp .loop           ; перейти на .loop
.end:
    ret

; Принимает указатель на нуль-терминированную строку, выводит её в stdout
print_string:
    push rdi            ; запись rdi в стек
    call string_length  ; вызов string_length
    mov rdx,rax         ; rax->rdx
    pop rsi             ; выводим из стека записаное ранее rdi в rsi
    mov rax,1           
    mov rdi,1
    syscall
    ret

; Принимает код символа и выводит его в stdout
print_char:
    push rdi      ; запись rdi в стек
    mov rdx,1     ; 1->rdx
    mov rsi,rsp   ; rsp->rsi
    mov rax,1     ; 1->rax
    mov rdi,1     ; 1->rdi
    syscall
    pop rdi
    ret

section .data
newline: db 0xA
section .text
; Переводит строку (выводит символ с кодом 0xA)
print_newline:
    mov rdi, newline  ; newline->rdi
    call print_char   ; вызов функции print_char
    ret

; Выводит беззнаковое 8-байтовое число в десятичном формате
; Совет: выделите место в стеке и храните там результаты деления
; Не забудьте перевести цифры в их ASCII коды.
print_uint:
    mov rax, rdi       ; rdi->rax
    mov rcx, 10        ; 10->rcx
    dec rsp            ; rsp-1->rsp
    xor rdx,rdx
    mov byte[rsp], 0   ; 0->byte[rsp]
    mov r10, 1
    .loop:             ; цикл loop пока rax!=0
        inc r10
        div rcx        ; деление на 10
        add rdx, '0'
        dec rsp        ; выделение памяти в стеке
        mov byte[rsp], dl   ; dl->byte[rsp]
        xor rdx,rdx
        test rax,rax   ; если rax!=0, то переход на .loop
        jne .loop
    .end:
        mov rdi, rsp    ; запись адреса начала строки
        push r10
        call print_string   ; вызов функции print_string
        pop r10
        add rsp, r10
        ret

; Выводит знаковое 8-байтовое число в десятичном формате
print_int:
    cmp rdi,0        ; если rdi!=0, то вызов parse_uint
    jnl print_uint
    push rdi
    mov rdi, '-'     ; вывод '-' благодаря функции print_char
    call print_char  
    pop rdi
    neg rdi          ; -1*rdi->rdi
    call print_uint  ; вызов функции parse_uint
    ret

; Принимает два указателя на нуль-терминированные строки, возвращает 1 если они равны, 0 иначе
string_equals:
    xor rax,rax
    .loop:
        mov al, byte[rdi]   
        cmp al, byte[rsi] ;сравнение байта из rdi и из rsi, если не равно, то переход на .no
        jne .no
        inc rdi
        inc rsi
        test al, al       ; проверка, что al!=0, если да, то переход на .loop 
        jne .loop
        mov rax, 1        ; 1->rdx
        ret
    .no:
        xor rax, rax      ; 0->rdx
        ret

; Читает один символ из stdin и возвращает его. Возвращает 0 если достигнут конец потока
read_char:
    xor rax,rax       
    xor rdi,rdi
    dec rsp            ; rsp-1->rsp 
    mov byte[rsp],0    ; 0->byte[rsp]
    mov rdx,1
    mov rsi,rsp
    syscall
    mov al,byte[rsp]   ; byte[rsp]->al
    inc rsp
    ret

; Принимает: адрес начала буфера, размер буфера
; Читает в буфер слово из stdin, пропуская пробельные символы в начале, .
; Пробельные символы это пробел 0x20, табуляция 0x9 и перевод строки 0xA.
; Останавливается и возвращает 0 если слово слишком большое для буфера
; При успехе возвращает адрес буфера в rax, длину слова в rdx.
; При неудаче возвращает 0 в rax
; Эта функция должна дописывать к слову нуль-терминатор

read_word:
    push r12
    push r13
    push r14
    mov r12, rdi
    mov r13, rsi
    xor r14, r14
    .loop:
        call read_char      ; вызов функции read_char
        test rax, rax
        jz .yes             ; если символ равен 0, то переход на .yes
        cmp r14, r13
        jnl .great          ; если r14>=r13, то переход на .great
        cmp rax, 0xA        
        jz .space           ; проверка на пробельный символ
        cmp rax, 0x9
        jz .space           ; проверка на пробельный символ
        cmp rax, 0x20
        jz .space           ; проверка на пробельный символ
        mov [r12 + r14], al   ; запись символа в память
        inc r14
        jmp .loop
    .yes:
         mov byte[r14+r12],0   ; запись нуль-терминированного элемента
         mov rax, r12          
         mov rdx, r14
         jmp .end
    .space:
         test r14,r14          ; проверка на то что r14==0
         je .loop
         jmp .yes
    .great:
        xor rax, rax           ; 0->rax
    .end:
        pop r14
        pop r13
        pop r12
        ret
 

; Принимает указатель на строку, пытается
; прочитать из её начала беззнаковое число.
; Возвращает в rax: число, rdx : его длину в символах
; rdx = 0 если число прочитать не удалось
parse_uint:
    xor rdx, rdx
    xor rax, rax
    xor r11, r11
    .loop:
        mov r11b, byte [rdi + rdx]  ; byte[rdi+rdx]->r11b
        test r11b, r11b             ; проверка r11b на 0
        je .end
        cmp r11b, '0'               ; проверка, что r11b<0
        jb .end
        cmp r11b, '9'               ; проверка, что r11b>9
        ja .end
        push rdx
        mov rdx,10
        mul rdx                     ; rax//10->rax
        pop rdx
        sub r11b, '0'
        add rax, r11                 ; rax+r11b->rax
        inc rdx
        jmp .loop
    .end:
        ret




; Принимает указатель на строку, пытается
; прочитать из её начала знаковое число.
; Если есть знак, пробелы между ним и числом не разрешены.
; Возвращает в rax: число, rdx : его длину в символах (включая знак, если он был)
; rdx = 0 если число прочитать не удалось
parse_int:
    cmp byte[rdi], '-'  ; проверка на то что первый символ '-'
    je .noequal
    cmp bl, '+'         ; проверка на то что первый символ '+'
    je .noequal
    call parse_uint     ; вызов функции parse_uint
    jmp .end
    .noequal:
        inc rdi
        push rdi
        call parse_uint   ; вызов функции parse_uint
        pop rdi
        test rdx,rdx      ; проверка на rdx==0
        je .end
        neg rax           ; -1*rax->rax
        inc rdx
    .end:
        ret

; Принимает указатель на строку, указатель на буфер и длину буфера
; Копирует строку в буфер
; Возвращает длину строки если она умещается в буфер, иначе 0
string_copy:
    xor rcx,rcx
    .loop:
        cmp rcx,rdx     ; проверка, что длина строки меньше rdx
        je .no
        mov r11b,[rdi]
        mov [rsi], r11b ; [rdi]->[rsi]
        inc rdi
        inc rsi
        inc rcx
        test r11b,r11b  ; проверка r11b на 0
        je .yes
        jmp .loop
    .no:
        xor rax,rax
        ret
    .yes:
        mov byte[rsi], 0 ; добавление нуль-терминированного элемента
        mov rax, rcx
        ret
